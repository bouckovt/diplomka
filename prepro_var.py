from tables import *
import prepro_fce
# ziskani indexu pouzitim std filtru po vyrazeni 0/1 konstantnich sloupecku

data_file = open_file("data.h5", mode="a")

train_phys_chem = data_file.root.train_set.phys_chem
train_mor1 = data_file.root.train_set.mor1
train_mor2 = data_file.root.train_set.mor2
train_mor3 = data_file.root.train_set.mor3
train_mor1f = data_file.root.train_set.mor1f
train_mor2f = data_file.root.train_set.mor2f
train_mor3f = data_file.root.train_set.mor3f
train_atom_pairs = data_file.root.train_set.atom_pairs
train_topo_torsion = data_file.root.train_set.topo_torsion
train_topo = data_file.root.train_set.topo
train_maccs = data_file.root.train_set.maccs
train_structural = data_file.root.train_set.structural

test_phys_chem = data_file.root.test_set.phys_chem
test_mor1 = data_file.root.test_set.mor1
test_mor2 = data_file.root.test_set.mor2
test_mor3 = data_file.root.test_set.mor3
test_mor1f = data_file.root.test_set.mor1f
test_mor2f = data_file.root.test_set.mor2f
test_mor3f = data_file.root.test_set.mor3f
test_atom_pairs = data_file.root.test_set.atom_pairs
test_topo_torsion = data_file.root.test_set.topo_torsion
test_topo = data_file.root.test_set.topo
test_maccs = data_file.root.test_set.maccs
test_structural = data_file.root.test_set.structural


group_prepro = data_file.create_group(data_file.root, 'prepro', 'Prepro indexes data')

# vyrazeni konstantnich sloupecku
a = prepro_fce.variance(train_mor1, test_mor1)
data_file.create_array(group_prepro, 'var_mor1', a, "Indexes of columns without full 0/1 for mor1")
print "done with mor1"

a = prepro_fce.variance(train_mor2, test_mor2)
data_file.create_array(group_prepro, 'var_mor2', a, "Indexes of columns without full 0/1 for mor2")
print "done with mor2"

a = prepro_fce.variance(train_mor3, test_mor3)
data_file.create_array(group_prepro, 'var_mor3', a, "Indexes of columns without full 0/1 for mor3")
print "done with mor3"

a = prepro_fce.variance(train_mor1f, test_mor1f)
data_file.create_array(group_prepro, 'var_mor1f', a, "Indexes of columns without full 0/1 for mor1f")
print "done with mor1f"

a = prepro_fce.variance(train_mor2f, test_mor2f)
data_file.create_array(group_prepro, 'var_mor2f', a, "Indexes of columns without full 0/1 for mor2f")
print "done with mor2f"

a = prepro_fce.variance(train_mor3f, test_mor3f)
data_file.create_array(group_prepro, 'var_mor3f', a, "Indexes of columns without full 0/1 for mor3f")
print "done with mor3f"

a = prepro_fce.variance(train_atom_pairs, test_atom_pairs)
data_file.create_array(group_prepro, 'var_atom_pairs', a, "Indexes of columns without full 0/1 for atom pairs")
print "done with atom_pairs"

a = prepro_fce.variance(train_topo_torsion, test_topo_torsion)
data_file.create_array(group_prepro, 'var_topo_torsion', a, "Indexes of columns without full 0/1 for topo_torsion")
print "done with topo torsion"

a = prepro_fce.variance(train_topo, test_topo)
data_file.create_array(group_prepro, 'var_topo', a, "Indexes of columns without full 0/1 for topo")
print "done with topo"

a = prepro_fce.variance(train_maccs, test_maccs)
data_file.create_array(group_prepro, 'var_maccs', a, "Indexes of columns without full 0/1 for maccs")
print "done with maccs"

a = prepro_fce.variance2(train_structural, test_structural)
data_file.create_array(group_prepro, 'var_structural', a, "Indexes of columns without full 0/1 for structural")
print "done with structural"



data_file.close()