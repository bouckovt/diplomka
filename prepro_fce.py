import numpy as np
from sklearn.feature_selection import VarianceThreshold

# prepro funkce

# pro MQN, MACCS, other fp
def corr(train_set_data, test_set_dat, indexes):
    pokus = np.vstack((train_set_data[:,indexes], test_set_dat[:,indexes]))


    pokus = np.transpose(pokus)


    cor = np.corrcoef(pokus)


    lower_tri = np.tril(cor,k=-1)
    higher = np.where(lower_tri > 0.98)
    lower = np.where(lower_tri < -0.98)



    indexes_to_keep = np.delete(indexes, higher[0],0)
    indexes_to_keep = np.delete(indexes_to_keep, lower[0], 0)

    return indexes_to_keep

# na celou train + test mnozinu
# vsechny fp, mqn
# bude provadet rovnout filter na stejne sloupecky 0 nebo
def variance(train_set_data, test_set_data):
    ids = []
    # pocet sloupcu v poli

    number_of_columns = train_set_data[0,:].shape[0]
    for i in range(number_of_columns):
        a = np.sum(train_set_data[:,i]) + np.sum(test_set_data[:,i])
        # pro fps
        if 0 < a < 477663:
            ids.append(i)

        # pro mqn
        #print i, a

    return ids

# pro MQN
def variance2(train_set_data, test_set_data):
    ids = []
    # pocet sloupcu v poli

    number_of_columns = train_set_data[0,:].shape[0]
    for i in range(number_of_columns):
        a = np.sum(train_set_data[:,i]) + np.sum(test_set_data[:,i])

        if 0 < a:
            ids.append(i)

    return ids
# mqn + chiral
def std(train_set_data, test_set_data, indexes):
    ids = []


    for i in indexes:
        a = np.std(np.append(train_set_data[:,i], test_set_data[:,i]))
        if a >= 1:
            ids.append(i)

    return ids


