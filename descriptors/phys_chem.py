from rdkit.Chem import Descriptors
from rdkit.Chem import Crippen

#molekularni hmotnost
def mol_wt(molecule):
    return Descriptors.MolWt(molecule)

#logP
def logP(molecule):
    return Crippen.MolLogP(molecule)

#otacivost
def mol_mr(molecule):
    return Crippen.MolMR(molecule)



