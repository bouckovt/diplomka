from rdkit import Chem
from rdkit.Chem import rdMolDescriptors

def structural_desc(molecule):
    m = mqn(molecule)
    ch = number_chiral_centers(molecule)
    m.append(ch)
    return m

def mqn(molecule):
    #mozna konverze sdf to mol
    MQN = rdMolDescriptors.MQNs_(molecule)
    return MQN

def number_chiral_centers(molecule):
    chiral_centers = len(Chem.FindMolChiralCenters(molecule, includeUnassigned=True))
    return chiral_centers