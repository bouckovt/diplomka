from rdkit.Chem import AllChem
from rdkit.Chem.AtomPairs import Pairs
from rdkit.Chem.AtomPairs import Torsions
from rdkit.Chem.Fingerprints import FingerprintMols
from rdkit.Chem import MACCSkeys


def morgan_fp(molecule, radius, morgan_feature_switch=False):

    fp = AllChem.GetMorganFingerprintAsBitVect(molecule, radius, nBits=1024, useFeatures=morgan_feature_switch)
    #toBitString
    return fp.ToBitString()


def atom_pair_fp(molecule):
    fp = Pairs.GetHashedAtomPairFingerprint(molecule)
    fp_bits = fp.GetNonzeroElements()
    fp_bit_string = ["0" for bit in range(fp.GetLength())]
    for key in fp_bits:
        fp_bit_string[key] = "1"
    return "".join(fp_bit_string)



def topological_torsion_fp(molecule):
    fp = Torsions.GetHashedTopologicalTorsionFingerprint(molecule) #default delka 2048 bitu
    fp_bits = fp.GetNonzeroElements()
    fp_bitString = ["0" for bit in range(fp.GetLength())]
    for key in fp_bits:
        fp_bitString[key] = "1"
    return "".join(fp_bitString)


def topological_fp(molecule):
    fp = FingerprintMols.FingerprintMol(molecule, minPath=1, maxPath=7, bitsPerHash=2, useHs=True, tgtDensity=0.0, minSize=128, fpSize = 2048)
    #toBitString
    return fp.ToBitString()


def maccs_fp(molecule):
    fp = MACCSkeys.GenMACCSKeys(molecule)
    #toBitString
    return fp.ToBitString()