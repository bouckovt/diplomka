from tables import *
import prepro_fce
# ziskani indexu pouzitim std filtru po vyrazeni 0/1 konstantnich sloupecku

data_file = open_file("data.h5", mode="a")

train_phys_chem = data_file.root.train_set.phys_chem
train_mor1 = data_file.root.train_set.mor1
train_mor2 = data_file.root.train_set.mor2
train_mor3 = data_file.root.train_set.mor3
train_mor1f = data_file.root.train_set.mor1f
train_mor2f = data_file.root.train_set.mor2f
train_mor3f = data_file.root.train_set.mor3f
train_atom_pairs = data_file.root.train_set.atom_pairs
train_topo_torsion = data_file.root.train_set.topo_torsion
train_topo = data_file.root.train_set.topo
train_maccs = data_file.root.train_set.maccs
train_structural = data_file.root.train_set.structural

test_phys_chem = data_file.root.test_set.phys_chem
test_mor1 = data_file.root.test_set.mor1
test_mor2 = data_file.root.test_set.mor2
test_mor3 = data_file.root.test_set.mor3
test_mor1f = data_file.root.test_set.mor1f
test_mor2f = data_file.root.test_set.mor2f
test_mor3f = data_file.root.test_set.mor3f
test_atom_pairs = data_file.root.test_set.atom_pairs
test_topo_torsion = data_file.root.test_set.topo_torsion
test_topo = data_file.root.test_set.topo
test_maccs = data_file.root.test_set.maccs
test_structural = data_file.root.test_set.structural


group_prepro = data_file.create_group(data_file.root, 'prepro2', 'Prepro indexes data with var and cor')

indexes = data_file.root.prepro.var_mor1[:]
a = prepro_fce.corr(train_mor1, test_mor1, indexes)
data_file.create_array(group_prepro, 'cor_var_mor1', a, "Indexes of columns without full 0/1 for mor1 and cor")
print "done with mor1"

indexes = data_file.root.prepro.var_mor2[:]
a = prepro_fce.corr(train_mor2, test_mor2, indexes)
data_file.create_array(group_prepro, 'cor_var_mor2', a, "Indexes of columns without full 0/1 for mor2 and cor")
print "done with mor2"

indexes = data_file.root.prepro.var_mor3[:]
a = prepro_fce.corr(train_mor3, test_mor3, indexes)
data_file.create_array(group_prepro, 'cor_var_mor3', a, "Indexes of columns without full 0/1 for mor3 and cor")
print "done with mor3"

indexes = data_file.root.prepro.var_mor1f[:]
a = prepro_fce.corr(train_mor1f, test_mor1f, indexes)
data_file.create_array(group_prepro, 'cor_var_mor1f', a, "Indexes of columns without full 0/1 for mor1f and cor")
print "done with mor1f"

indexes = data_file.root.prepro.var_mor2f[:]
a = prepro_fce.corr(train_mor2f, test_mor2f, indexes)
data_file.create_array(group_prepro, 'cor_var_mor2f', a, "Indexes of columns without full 0/1 for mor2f and cor")
print "done with mor2f"

indexes = data_file.root.prepro.var_mor3f[:]
a = prepro_fce.corr(train_mor3f, test_mor3f, indexes)
data_file.create_array(group_prepro, 'cor_var_mor3f', a, "Indexes of columns without full 0/1 for mor3f and cor")
print "done with mor3f"

indexes = data_file.root.prepro.var_atom_pairs[:]
a = prepro_fce.corr(train_atom_pairs, test_atom_pairs, indexes)
data_file.create_array(group_prepro, 'cor_var_atom_pairs', a, "Indexes of columns without full 0/1 for atom pairs and cor")
print "done with atom_pairs"

indexes = data_file.root.prepro.var_topo_torsion[:]
a = prepro_fce.corr(train_topo_torsion, test_topo_torsion, indexes)
data_file.create_array(group_prepro, 'cor_var_topo_torsion', a, "Indexes of columns without full 0/1 for topo_torsion and cor")
print "done with topo torsion"

indexes = data_file.root.prepro.var_topo[:]
a = prepro_fce.corr(train_topo, test_topo, indexes)
data_file.create_array(group_prepro, 'cor_var_topo', a, "Indexes of columns without full 0/1 for topo and cor")
print "done with topo"

indexes = data_file.root.prepro.var_maccs[:]
a = prepro_fce.corr(train_maccs, test_maccs, indexes)
data_file.create_array(group_prepro, 'cor_var_maccs', a, "Indexes of columns without full 0/1 for maccs and cor")
print "done with maccs"

indexes = data_file.root.prepro.var_structural[:]
a = prepro_fce.corr(train_structural, test_structural, indexes)
data_file.create_array(group_prepro, 'cor_var_structural', a, "Indexes of columns without full 0/1 for structural and cor")
print "done with structural"



data_file.close()