import cPickle as pickle
# funkce pro ulozeni poctu a klicu sloucenin a duplikaty

# funkce vraci pocet sloucenin NP a NonNP a jejich klice
def count_of_cmpds_keys(np, non_np):
    number_of_np = 0
    number_of_nonnp = 0
    np_keys = []
    nonnp_keys = []

    # zjisti pocet
    try:
        while True:
            i = pickle.load(np)
            np_keys.append(i.keys()[0])
            number_of_np += 1

    except EOFError:
        print "Konec souboru vypoctu poctu np"

    try:
        while True:
            j = pickle.load(non_np)
            nonnp_keys.append(j.keys()[0])
            number_of_nonnp += 1

    except EOFError:
        print "Konec souboru vypoctu poctu nonnp"

    return number_of_np, number_of_nonnp, np_keys, nonnp_keys



# vraci seznam id sloucenin ktere se vyskytuji v obou souborech np/nonnp
def duplicates_list(np_keys, nonnp_keys):
    #2261 duplicates

    p = 0

    duplicates = []
    for id_np in np_keys:
        if id_np in nonnp_keys:

            duplicates.append(id_np)
            p += 1



    print "duplicates done"
    return duplicates




