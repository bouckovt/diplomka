from sklearn.naive_bayes import GaussianNB
import numpy as np


# data sety - seznam tuplu (list fp, class)
def bayes(train_set, test_set, train_classes):

    # trenovaci data X data, Y tridy
    train_X = train_set
    train_Y = train_classes

    # test data
    test_X = test_set
    #test_Y = test_classes

    # klasifikace
    clf = GaussianNB()
    clf.fit(train_X, train_Y)

    #predikce
    probab = clf.predict_proba(test_X)
    class_predictions = clf.predict(test_X)



    return class_predictions, probab

