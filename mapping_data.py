import cPickle as pickle
from descriptors import fingerprinty, structural, phys_chem
from rdkit import Chem
import numpy as np
from tables import *
import tables

# vypocet deskriptoru a ulozeni do matic v pytables s udajema

train_set = open("train_set.pkl", "rb")
test_set = open("test_set.pkl", "rb")

# otevreni data souboru
data_file = open_file("data.h5", mode="a")

train_table_info = data_file.root.train_set.data_info
train_phys_chem = data_file.root.train_set.phys_chem
train_mor1 = data_file.root.train_set.mor1
train_mor2 = data_file.root.train_set.mor2
train_mor3 = data_file.root.train_set.mor3
train_mor1f = data_file.root.train_set.mor1f
train_mor2f = data_file.root.train_set.mor2f
train_mor3f = data_file.root.train_set.mor3f
train_atom_pairs = data_file.root.train_set.atom_pairs
train_topo_torsion = data_file.root.train_set.topo_torsion
train_topo = data_file.root.train_set.topo
train_maccs = data_file.root.train_set.maccs
train_structural = data_file.root.train_set.structural



i = 0
try:
    while True:
        mol = pickle.load(train_set)

        zinc_id = mol.keys()[0]
        smiles = mol[zinc_id]["molecule"]
        molecule = Chem.MolFromSmiles(smiles)
        class_mol = mol[zinc_id]["class"]

        # pridani info do tabulky

        info = train_table_info.row
        info['mol_class'] = class_mol
        info['zinc_id'] = zinc_id

        info.append()
        #physchem

        mol_wt = phys_chem.mol_wt(molecule)
        mol_mr = phys_chem.mol_mr(molecule)
        logP = phys_chem.logP(molecule)

        physch = np.array([mol_wt, mol_mr, logP], dtype=np.float32)
        train_phys_chem[i, :] = physch


        #morgan_fp
        morgan_rad1 = fingerprinty.morgan_fp(molecule, 1)
        train_mor1[i, :] = np.array(list(morgan_rad1), dtype=np.int8)

        morgan_rad2 = fingerprinty.morgan_fp(molecule, 2)
        train_mor2[i, :] = np.array(list(morgan_rad2), dtype=np.int8)

        morgan_rad3 = fingerprinty.morgan_fp(molecule, 3)
        train_mor3[i, :] = np.array(list(morgan_rad3), dtype=np.int8)


        morgan_rad1_f = fingerprinty.morgan_fp(molecule, 1, True)
        train_mor1f[i, :] = np.array(list(morgan_rad1_f), dtype=np.int8)

        morgan_rad2_f = fingerprinty.morgan_fp(molecule, 2, True)
        train_mor2f[i, :] = np.array(list(morgan_rad2_f), dtype=np.int8)

        morgan_rad3_f = fingerprinty.morgan_fp(molecule, 3, True)
        train_mor3f[i, :] = np.array(list(morgan_rad3_f), dtype=np.int8)

        #atompair

        atom_pair = fingerprinty.atom_pair_fp(molecule)
        train_atom_pairs[i, :] = np.array(list(atom_pair), dtype=np.int8)

        #topo torsion
        topo_torsion_fp = fingerprinty.topological_torsion_fp(molecule)
        train_topo_torsion[i, :] = np.array(list(topo_torsion_fp), dtype=np.int8)

        #topological_fp
        topological_fp = fingerprinty.topological_fp(molecule)
        train_topo[i, :] = np.array(list(topological_fp), dtype=np.int8)

        #maccs
        maccs = fingerprinty.maccs_fp(molecule)
        train_maccs[i, :] = np.array(list(maccs), dtype=np.int8)

        #mqn + number of chiral centers
        struct = structural.structural_desc(molecule)
        train_structural[i, :] = np.array(struct, dtype=np.int16)



        i += 1

except EOFError:
    print "Konec souboru train set souboru"

train_set.close()
train_table_info.flush()


test_table_info = data_file.root.test_set.data_info
test_phys_chem = data_file.root.test_set.phys_chem
test_mor1 = data_file.root.test_set.mor1
test_mor2 = data_file.root.test_set.mor2
test_mor3 = data_file.root.test_set.mor3
test_mor1f = data_file.root.test_set.mor1f
test_mor2f = data_file.root.test_set.mor2f
test_mor3f = data_file.root.test_set.mor3f
test_atom_pairs = data_file.root.test_set.atom_pairs
test_topo_torsion = data_file.root.test_set.topo_torsion
test_topo = data_file.root.test_set.topo
test_maccs = data_file.root.test_set.maccs
test_structural = data_file.root.test_set.structural

b = 0
try:
    while True:
        mol = pickle.load(test_set)

        zinc_id = mol.keys()[0]
        smiles = mol[zinc_id]["molecule"]
        molecule = Chem.MolFromSmiles(smiles)
        class_mol = mol[zinc_id]["class"]

        info = test_table_info.row
        info['mol_class'] = class_mol
        info['zinc_id'] = zinc_id

        info.append()

        #physchem

        mol_wt = phys_chem.mol_wt(molecule)
        mol_mr = phys_chem.mol_mr(molecule)
        logP = phys_chem.logP(molecule)

        physch = np.array([mol_wt, mol_mr, logP], dtype=np.float32)
        test_phys_chem[b, :] = physch


        #morgan_fp
        morgan_rad1 = fingerprinty.morgan_fp(molecule, 1)
        test_mor1[b, :] = np.array(list(morgan_rad1), dtype=np.int8)

        morgan_rad2 = fingerprinty.morgan_fp(molecule, 2)
        test_mor2[b, :] = np.array(list(morgan_rad2), dtype=np.int8)

        morgan_rad3 = fingerprinty.morgan_fp(molecule, 3)
        test_mor3[b, :] = np.array(list(morgan_rad3), dtype=np.int8)


        morgan_rad1_f = fingerprinty.morgan_fp(molecule, 1, True)
        test_mor1f[b, :] = np.array(list(morgan_rad1_f), dtype=np.int8)

        morgan_rad2_f = fingerprinty.morgan_fp(molecule, 2, True)
        test_mor2f[b, :] = np.array(list(morgan_rad2_f), dtype=np.int8)

        morgan_rad3_f = fingerprinty.morgan_fp(molecule, 3, True)
        test_mor3f[b, :] = np.array(list(morgan_rad3_f), dtype=np.int8)

        #atompair

        atom_pair = fingerprinty.atom_pair_fp(molecule)
        test_atom_pairs[b, :] = np.array(list(atom_pair), dtype=np.int8)

        #topo torsion
        topo_torsion_fp = fingerprinty.topological_torsion_fp(molecule)
        test_topo_torsion[b, :] = np.array(list(topo_torsion_fp), dtype=np.int8)

        #topological_fp
        topological_fp = fingerprinty.topological_fp(molecule)
        test_topo[b, :] = np.array(list(topological_fp), dtype=np.int8)

        #maccs
        maccs = fingerprinty.maccs_fp(molecule)
        test_maccs[b, :] = np.array(list(maccs), dtype=np.int8)

        #mqn + number of chiral centers
        struct = structural.structural_desc(molecule)
        test_structural[b, :] = np.array(struct, dtype=np.int16)
        b += 1

except EOFError:
    print "Konec souboru test set souboru"

test_set.close()
test_table_info.flush()

data_file.close()
print "pocet train set", i
print "pocet test set", b