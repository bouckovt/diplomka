import numpy as np
from classificators import bayes
from tables import *
import tables
from rdkit.ML.Scoring import Scoring
from operator import itemgetter

# vypocet ef a bedroc

data_file = open_file("data.h5", mode="a")


train_phys_chem = data_file.root.train_set.phys_chem
train_mor1 = data_file.root.train_set.mor1
train_mor2 = data_file.root.train_set.mor2
train_mor3 = data_file.root.train_set.mor3
train_mor1f = data_file.root.train_set.mor1f
train_mor2f = data_file.root.train_set.mor2f
train_mor3f = data_file.root.train_set.mor3f
train_atom_pairs = data_file.root.train_set.atom_pairs
train_topo_torsion = data_file.root.train_set.topo_torsion
train_topo = data_file.root.train_set.topo
train_maccs = data_file.root.train_set.maccs
train_structural = data_file.root.train_set.structural

test_phys_chem = data_file.root.test_set.phys_chem
test_mor1 = data_file.root.test_set.mor1
test_mor2 = data_file.root.test_set.mor2
test_mor3 = data_file.root.test_set.mor3
test_mor1f = data_file.root.test_set.mor1f
test_mor2f = data_file.root.test_set.mor2f
test_mor3f = data_file.root.test_set.mor3f
test_atom_pairs = data_file.root.test_set.atom_pairs
test_topo_torsion = data_file.root.test_set.topo_torsion
test_topo = data_file.root.test_set.topo
test_maccs = data_file.root.test_set.maccs
test_structural = data_file.root.test_set.structural

index_phys_chem = range(3)
index_var_mor1 = data_file.root.prepro.var_mor1[:]
index_var_mor2 = data_file.root.prepro.var_mor2[:]
index_var_mor3 = data_file.root.prepro.var_mor3[:]
index_var_mor1f = data_file.root.prepro.var_mor1f[:]
index_var_mor2f = data_file.root.prepro.var_mor2f[:]
index_var_mor3f = data_file.root.prepro.var_mor3f[:]
index_var_atom_pairs = data_file.root.prepro.var_atom_pairs[:]
index_var_topo_torsion = data_file.root.prepro.var_topo_torsion[:]
index_var_topo = data_file.root.prepro.var_topo[:]
index_var_maccs = data_file.root.prepro.var_maccs[:]
index_var_structural = data_file.root.prepro.var_structural[:]

train_data_info = data_file.root.train_set.data_info
train_classes = train_data_info.cols.mol_class[:]

test_data_info = data_file.root.test_set.data_info
test_classes = test_data_info.cols.mol_class[:]

train_data = [train_phys_chem, train_mor1, train_mor2, train_mor3, train_mor1f, train_mor2f, train_mor3f,
             train_atom_pairs, train_topo_torsion, train_topo, train_maccs, train_structural]

test_data = [test_phys_chem, test_mor1, test_mor2, test_mor3, test_mor1f, test_mor2f, test_mor3f, test_atom_pairs,
             test_topo_torsion, test_topo, test_maccs, test_structural]

names = ["phys_chem", "mor1", "mor2", "mor3", "mor1f", "mor2f", "mor3f", "atom_pairs", "topo_torsion",
         "topo", "maccs", "structural"]

indexes1 = [index_phys_chem, index_var_mor1, index_var_mor2, index_var_mor3, index_var_mor1f, index_var_mor2f,
            index_var_mor3f, index_var_atom_pairs, index_var_topo_torsion, index_var_topo, index_var_maccs,
            index_var_structural]

# parametry pro EF a BEDROC
ef_fractions = [0.01, 0.05, 0.1, 0.2, 0.3, 0.4]
bedroc_alpha = [100, 20, 10, 5, 3, 2]

class Ef(IsDescription):
    name_of_descriptor = tables.StringCol(30)
    ef1 = tables.Float32Col()
    ef5 = tables.Float32Col()
    ef10 = tables.Float32Col()
    ef20 = tables.Float32Col()
    ef30 = tables.Float32Col()
    ef40 = tables.Float32Col()

class Bedroc(IsDescription):
    name_of_descriptor = tables.StringCol(30)
    bedroc100 = tables.Float32Col()
    bedroc20 = tables.Float32Col()
    bedroc10 = tables.Float32Col()
    bedroc5 = tables.Float32Col()
    bedroc3 = tables.Float32Col()
    bedroc2 = tables.Float32Col()

group_results = data_file.root.results
res_table_ef = data_file.create_table(group_results, "enrichment_factor", Ef, "enrichment factors")
res_table_bedroc = data_file.create_table(group_results, "bedroc", Bedroc, "bedroc factor")

bedroc_cols = ["bedroc100", "bedroc20", "bedroc10", "bedroc5", "bedroc3", "bedroc2"]

for i in range(len(train_data)):
    row_ef = res_table_ef.row
    row_bedroc = res_table_bedroc.row

    row_ef["name_of_descriptor"] = names[i]
    row_bedroc["name_of_descriptor"] = names[i]

    train_X = train_data[i][:,indexes1[i]]
    test_X= test_data[i][:,indexes1[i]]
    class_predictions = bayes.bayes(train_X, test_X, train_classes)[0]
    probab = bayes.bayes(train_X, test_X, train_classes)[1]


    tup_list = []
    for i in range(len(test_classes)):
        #print probab[i], class_predictions[i], test_classes[i]
        tup_list.append((test_classes[i], probab[i][1]))


    sorted_list = sorted(tup_list,key=itemgetter(1), reverse=True)
    #print sorted_list[45000:45050]
    active_info = 0
    ef = Scoring.CalcEnrichment(sorted_list, active_info, ef_fractions)

    row_ef["ef1"] = ef[0]
    row_ef["ef5"] = ef[1]
    row_ef["ef10"] = ef[2]
    row_ef["ef20"] = ef[3]
    row_ef["ef30"] = ef[4]
    row_ef["ef40"] = ef[5]
    #print ef
    row_ef.append()

    for i, al in enumerate(bedroc_alpha):
        bedroc = Scoring.CalcBEDROC(sorted_list, active_info, al)
        #print bedroc
        row_bedroc[bedroc_cols[i]] = bedroc

    row_bedroc.append()

res_table_bedroc.flush()
res_table_ef.flush()
data_file.close()