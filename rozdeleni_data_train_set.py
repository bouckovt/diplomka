
import cPickle as pickle
import utils

# rozdeleni dat do train a test setu


np = open("data/NPcmds.pkl", "rb")
non_np = open("data/nonNPcmds.pkl", "rb")

number_of_np, number_of_nonnp, np_keys, nonnp_keys = utils.count_of_cmpds_keys(np, non_np)

duplicates = utils.duplicates_list(np_keys, nonnp_keys)

np.close()
non_np.close()

print "duplicates", len(duplicates)
print "finnished counting data"
np = open("data/NPcmds.pkl", "rb")
non_np = open("data/nonNPcmds.pkl", "rb")
train_set = open("train_set.pkl", "wb")
test_set = open("test_set.pkl", "wb")

k = 0

print "start rozdeleni dat do setu"
pocet_NP_sloucenin = 0
pocet_NP_train = 0
pocet_NP_test = 0

try:
    while True:
        mol_details = pickle.load(np)
        if k <= 0.7*number_of_np:

            pickle.dump(mol_details, train_set, -1)

            k += 1
            pocet_NP_sloucenin += 1
            pocet_NP_train += 1
        else:


            pickle.dump(mol_details, test_set, -1)
            k += 1
            pocet_NP_sloucenin += 1
            pocet_NP_test += 1

except EOFError:
    print "Konec souboru NP souboru"


print "pocet NP sl", pocet_NP_sloucenin
print "pocet NP test", pocet_NP_test
print "pocet NP train", pocet_NP_train

# pridani nonNP do train/test setu
l = 0
pocet_nonNP_sloucenin = 0
pocet_nonNP_train = 0
pocet_nonNP_test = 0

try:
    while True:
        mol_details = pickle.load(non_np)
        id_key = mol_details.keys()[0]

        if id_key not in duplicates:
            if l <= 0.7*(number_of_nonnp - len(duplicates)):

                pickle.dump(mol_details, train_set, -1)
                l += 1
                pocet_nonNP_sloucenin += 1
                pocet_nonNP_train += 1
            else:

                pickle.dump(mol_details, test_set, -1)
                l += 1
                pocet_nonNP_sloucenin += 1
                pocet_nonNP_test += 1
        else:
            pass

except EOFError:
    print "Konec souboru nonNP souboru"

print "pocet nonNP sl", pocet_nonNP_sloucenin
print "pocet noonNP test", pocet_nonNP_test
print "pocet nonNP train", pocet_nonNP_train


train_set.close()
test_set.close()
np.close()
non_np.close()

#duplicates 2253
#finnished counting data
#start rozdeleni dat do setu
#Konec souboru NP souboru
#pocet NP sl 179916
#pocet NP test 53974
#pocet NP train 125942
#Konec souboru nonNP souboru
#pocet nonNP sl 297747
#pocet noonNP test 89324
#pocet nonNP train 208423
