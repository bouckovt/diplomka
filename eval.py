import numpy as np
from tables import *
from classificators import bayes
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from tables import *
import tables
# vypocet accuray, precision, recall, f1 miry

data_file = open_file("data.h5", mode="a")


train_phys_chem = data_file.root.train_set.phys_chem
train_mor1 = data_file.root.train_set.mor1
train_mor2 = data_file.root.train_set.mor2
train_mor3 = data_file.root.train_set.mor3
train_mor1f = data_file.root.train_set.mor1f
train_mor2f = data_file.root.train_set.mor2f
train_mor3f = data_file.root.train_set.mor3f
train_atom_pairs = data_file.root.train_set.atom_pairs
train_topo_torsion = data_file.root.train_set.topo_torsion
train_topo = data_file.root.train_set.topo
train_maccs = data_file.root.train_set.maccs
train_structural = data_file.root.train_set.structural

test_phys_chem = data_file.root.test_set.phys_chem
test_mor1 = data_file.root.test_set.mor1
test_mor2 = data_file.root.test_set.mor2
test_mor3 = data_file.root.test_set.mor3
test_mor1f = data_file.root.test_set.mor1f
test_mor2f = data_file.root.test_set.mor2f
test_mor3f = data_file.root.test_set.mor3f
test_atom_pairs = data_file.root.test_set.atom_pairs
test_topo_torsion = data_file.root.test_set.topo_torsion
test_topo = data_file.root.test_set.topo
test_maccs = data_file.root.test_set.maccs
test_structural = data_file.root.test_set.structural

index_phys_chem = range(3)
index_var_mor1 = data_file.root.prepro.var_mor1[:]
index_var_mor2 = data_file.root.prepro.var_mor2[:]
index_var_mor3 = data_file.root.prepro.var_mor3[:]
index_var_mor1f = data_file.root.prepro.var_mor1f[:]
index_var_mor2f = data_file.root.prepro.var_mor2f[:]
index_var_mor3f = data_file.root.prepro.var_mor3f[:]
index_var_atom_pairs = data_file.root.prepro.var_atom_pairs[:]
index_var_topo_torsion = data_file.root.prepro.var_topo_torsion[:]
index_var_topo = data_file.root.prepro.var_topo[:]
index_var_maccs = data_file.root.prepro.var_maccs[:]
index_var_structural = data_file.root.prepro.var_structural[:]



train_data_info = data_file.root.train_set.data_info
train_classes = train_data_info.cols.mol_class[:]

test_data_info = data_file.root.test_set.data_info
test_classes = test_data_info.cols.mol_class[:]

train_data = [train_phys_chem, train_mor1, train_mor2, train_mor3, train_mor1f, train_mor2f, train_mor3f,
             train_atom_pairs, train_topo_torsion, train_topo, train_maccs, train_structural]

test_data = [test_phys_chem, test_mor1, test_mor2, test_mor3, test_mor1f, test_mor2f, test_mor3f, test_atom_pairs,
             test_topo_torsion, test_topo, test_maccs, test_structural]

names = ["phys_chem", "mor1", "mor2", "mor3", "mor1f", "mor2f", "mor3f", "atom_pairs", "topo_torsion",
         "topo", "maccs", "structural"]

indexes1 = [index_phys_chem, index_var_mor1, index_var_mor2, index_var_mor3, index_var_mor1f, index_var_mor2f,
            index_var_mor3f, index_var_atom_pairs, index_var_topo_torsion, index_var_topo, index_var_maccs,
            index_var_structural]


class Info(IsDescription):
    name_of_descriptor = tables.StringCol(30)
    accuracy = tables.Float32Col()
    precision_score = tables.Float32Col()
    recall_score = tables.Float32Col()
    f1_score = tables.Float32Col()

group_results = data_file.create_group(data_file.root, 'results', 'results')
res_table = data_file.create_table(group_results, "bayes_eval", Info, "Bayes eval for var feature extraction")

for i in range(len(train_data)):
    row = res_table.row

    train_X = train_data[i][:,indexes1[i]]
    test_X= test_data[i][:,indexes1[i]]
    class_predictions = bayes.bayes(train_X, test_X, train_classes)[0]
    probab = bayes.bayes(train_X, test_X, train_classes)[1]

    # accuracy
    accuracy = accuracy_score(test_classes, class_predictions)

    # precision
    precision = precision_score(test_classes, class_predictions)

    # recall
    recall = recall_score(test_classes, class_predictions)

    # f1
    f1 = f1_score(test_classes, class_predictions)

    row["name_of_descriptor"] = names[i]
    row["accuracy"] = accuracy
    row["precision_score"] = precision
    row["recall_score"] = recall
    row["f1_score"] = f1
    row.append()

res_table.flush()

data_file.close()