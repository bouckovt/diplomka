import cPickle as pickle
from rdkit import Chem
import csv

np1 = Chem.SDMolSupplier("98_p0.0.sdf")
np2 = Chem.SDMolSupplier("98_p0.1.sdf")
np3 = Chem.SDMolSupplier("98_p1.0.sdf")

#198620

#
# pocet non duplicates 179916
# pocet duplicatu 18704
#
#

files_list = [np1, np2, np3]
ids = {}
out = open("NPcmds.pkl", "wb")
i = 0
d = 0
for file_cmdps in files_list:
    for cmpd in file_cmdps:
        if cmpd != None:
            id_cmpd = cmpd.GetProp("_Name")
            if id_cmpd not in ids.keys():
                ids[id_cmpd] = 1
                mol = {id_cmpd: {"molecule": Chem.MolToSmiles(cmpd), "class": 1}}
                pickle.dump(mol, out, -1)
                i += 1

            else:
                ids[id_cmpd] += 1
                d += 1
    print "konec souboru"

print "pocet non duplicates", i
print "pocet duplicatu", d
print len(ids.keys())


out.close()

