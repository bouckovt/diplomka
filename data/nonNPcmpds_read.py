import csv
from rdkit import Chem
import cPickle as pickle

out = open("nonNPcmds.pkl", "wb")
with open('zinc_random_300k.csv', 'rb') as csvfile:
    molreader = csv.reader(csvfile, delimiter=',', quotechar='|')
    i = 0
    ids = {}
    d = 0
    p = 0
    for row in molreader:
        if i != 0:
            id_cmpd = row[2]
            if id_cmpd not in ids.keys():
                mol = row[0]
                mol = {id_cmpd: {"molecule": mol, "class": 0}}
                pickle.dump(mol, out, -1)
                ids[id_cmpd] = 1
                p+= 1
            else:
                d+=1
                ids[id_cmpd]+=1

        else:
            i += 1

    print "pocet non duplicates", p
    print "pocet duplicatu", d
    print len(ids.keys())
out.close()