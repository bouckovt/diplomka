import numpy as np
from tables import *
from classificators import bayes
from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
# ROC grafy

def clf(train, test, indexes):
    train_X1 = train[:, indexes]
    test_X1= test[:, indexes]


    # klasifikace
    bay = bayes.bayes(train_X1, test_X1, train_classes)
    probab =  bay[1]


    fpr = dict()
    tpr = dict()
    roc_auc = dict()

    for i in range(2):

        fpr[i], tpr[i], _ = roc_curve(y[:, i], probab[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])

    return fpr, tpr, roc_auc

data_file = open_file("data.h5", mode="a")

train_phys_chem = data_file.root.train_set.phys_chem
train_mor1 = data_file.root.train_set.mor1
train_mor2 = data_file.root.train_set.mor2
train_mor3 = data_file.root.train_set.mor3
train_mor1f = data_file.root.train_set.mor1f
train_mor2f = data_file.root.train_set.mor2f
train_mor3f = data_file.root.train_set.mor3f
train_atom_pairs = data_file.root.train_set.atom_pairs
train_topo_torsion = data_file.root.train_set.topo_torsion
train_topo = data_file.root.train_set.topo
train_maccs = data_file.root.train_set.maccs
train_structural = data_file.root.train_set.structural

test_phys_chem = data_file.root.test_set.phys_chem
test_mor1 = data_file.root.test_set.mor1
test_mor2 = data_file.root.test_set.mor2
test_mor3 = data_file.root.test_set.mor3
test_mor1f = data_file.root.test_set.mor1f
test_mor2f = data_file.root.test_set.mor2f
test_mor3f = data_file.root.test_set.mor3f
test_atom_pairs = data_file.root.test_set.atom_pairs
test_topo_torsion = data_file.root.test_set.topo_torsion
test_topo = data_file.root.test_set.topo
test_maccs = data_file.root.test_set.maccs
test_structural = data_file.root.test_set.structural

index_phys_chem = range(3)
index_var_mor1 = data_file.root.prepro.var_mor1[:]
index_var_mor2 = data_file.root.prepro.var_mor2[:]
index_var_mor3 = data_file.root.prepro.var_mor3[:]
index_var_mor1f = data_file.root.prepro.var_mor1f[:]
index_var_mor2f = data_file.root.prepro.var_mor2f[:]
index_var_mor3f = data_file.root.prepro.var_mor3f[:]
index_var_atom_pairs = data_file.root.prepro.var_atom_pairs[:]
index_var_topo_torsion = data_file.root.prepro.var_topo_torsion[:]
index_var_topo = data_file.root.prepro.var_topo[:]
index_var_maccs = data_file.root.prepro.var_maccs[:]
index_var_structural = data_file.root.prepro.var_structural[:]



train_data_info = data_file.root.train_set.data_info
train_classes = train_data_info.cols.mol_class[:]

test_data_info = data_file.root.test_set.data_info
test_classes = test_data_info.cols.mol_class[:]


train_data = [train_phys_chem, train_mor1, train_mor2, train_mor3, train_mor1f, train_mor2f, train_mor3f,
             train_atom_pairs, train_topo_torsion, train_topo, train_maccs, train_structural]

test_data = [test_phys_chem, test_mor1, test_mor2, test_mor3, test_mor1f, test_mor2f, test_mor3f, test_atom_pairs,
             test_topo_torsion, test_topo, test_maccs, test_structural]


indexes1 = [index_phys_chem, index_var_mor1, index_var_mor2, index_var_mor3, index_var_mor1f, index_var_mor2f,
            index_var_mor3f, index_var_atom_pairs, index_var_topo_torsion, index_var_topo, index_var_maccs,
            index_var_structural]


names = ["physical-chemical descriptors",
         "ECFP2",
         "ECFP4",
         "ECFP6",
         "FCFP2",
         "FCFP4",
         "FCFP6",
         "atom pairs",
         "topological torsions",
         "RDKit",
         "MACCS",
         "MQN + number of chiral centers"]

colors = ["#FFFF00", "blue", "red", "green", "magenta", "cyan", "black", "#66FF66", "#FF9933", "#CC33FF", "#800000",
          "#9900CC"]
y = []
for i in test_classes:
    if i == 1:
        y.append([0,1])
    else:
        y.append([1,0])

y = np.array(y)
plt.figure()


fpr, tpr, roc_auc = clf(train_data[0], test_data[0], indexes1[0])
plt.plot(fpr[1], tpr[1], color="%s" % colors[0], label='%s (area = %0.2f)' % (names[0], roc_auc[1]))

fpr, tpr, roc_auc = clf(train_data[1], test_data[1], indexes1[1])
plt.plot(fpr[1], tpr[1], color="%s" % colors[1], label='%s (area = %0.2f)' % (names[1], roc_auc[1]))

fpr, tpr, roc_auc = clf(train_data[2], test_data[2], indexes1[2])
plt.plot(fpr[1], tpr[1], color="%s" % colors[2], label='%s (area = %0.2f)' % (names[2], roc_auc[1]))

fpr, tpr, roc_auc = clf(train_data[3], test_data[3], indexes1[3])
plt.plot(fpr[1], tpr[1], color="%s" % colors[3], label='%s (area = %0.2f)' % (names[3], roc_auc[1]))

fpr, tpr, roc_auc = clf(train_data[4], test_data[4], indexes1[4])
plt.plot(fpr[1], tpr[1], color="%s" % colors[4], label='%s (area = %0.2f)' % (names[4], roc_auc[1]))

fpr, tpr, roc_auc = clf(train_data[5], test_data[5], indexes1[5])
plt.plot(fpr[1], tpr[1], color="%s" % colors[5], label='%s (area = %0.2f)' % (names[5], roc_auc[1]))

fpr, tpr, roc_auc = clf(train_data[6], test_data[6], indexes1[6])
plt.plot(fpr[1], tpr[1], color="%s" % colors[6], label='%s (area = %0.2f)' % (names[6], roc_auc[1]))


fpr, tpr, roc_auc = clf(train_data[7], test_data[7], indexes1[7])
plt.plot(fpr[1], tpr[1], color="%s" % colors[7], label='%s (area = %0.2f)' % (names[7], roc_auc[1]))

fpr, tpr, roc_auc = clf(train_data[8], test_data[8], indexes1[8])
plt.plot(fpr[1], tpr[1], color="%s" % colors[8], label='%s (area = %0.2f)' % (names[8], roc_auc[1])) # TT

fpr, tpr, roc_auc = clf(train_data[9], test_data[9], indexes1[9])
plt.plot(fpr[1], tpr[1], color="%s" % colors[9], label='%s (area = %0.2f)' % (names[9], roc_auc[1]))

fpr, tpr, roc_auc = clf(train_data[10], test_data[10], indexes1[10])
plt.plot(fpr[1], tpr[1], color="%s" % colors[10], label='%s (area = %0.2f)' % (names[10], roc_auc[1]))

fpr, tpr, roc_auc = clf(train_data[11], test_data[11], indexes1[11])
plt.plot(fpr[1], tpr[1], color="%s" % colors[11], label='%s (area = %0.2f)' % (names[11], roc_auc[1]))

# atom pair color #66FF66
# topo torsion color #FF9933
# topo #CC33FF
# maccs #800000
# structural #9900CC
# phys_chem #FFFF00

plt.plot([0, 1], [0, 1], 'k--')
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate', fontsize=18)
plt.ylabel('True Positive Rate', fontsize=18)
plt.title('ROC for all descriptors', fontsize=20)
plt.legend(loc="lower right", prop={'size': 20})
plt.show()


data_file.close()