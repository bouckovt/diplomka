import numpy as np
from classificators import bayes
from tables import *
from operator import itemgetter
import matplotlib.pyplot as plt

data_file = open_file("data.h5", mode="a")

# vytvoreni grafu pro EC

def chunks(l, n):
    n = max(1, n)
    return [l[i:i + n] for i in range(0, len(l), n)]

def data(train, test, indexes):
    train_X = train[:,indexes]
    test_X= test[:,indexes]
    #class_predictions = bayes.bayes(train_X, test_X, train_classes)[0]
    probab = bayes.bayes(train_X, test_X, train_classes)[1]


    tup_list = []
    for i in range(len(test_classes)):
        #print probab[i], class_predictions[i], test_classes[i]
        tup_list.append((test_classes[i], probab[i][1]))


    sorted_list = sorted(tup_list,key=itemgetter(1), reverse=True)

    #print sorted_list
    chunk_list = chunks(sorted_list, 14330)
    #print chunk_list
    actives = [0]
    active_count = 0
    for chunk in chunk_list:

        for column in chunk:
            if column[0]:
                active_count += 1

        a = 100*active_count/float(53974)
        #print active_count
        actives.append(a)

    return actives

train_phys_chem = data_file.root.train_set.phys_chem
train_mor1 = data_file.root.train_set.mor1
train_mor2 = data_file.root.train_set.mor2
train_mor3 = data_file.root.train_set.mor3
train_mor1f = data_file.root.train_set.mor1f
train_mor2f = data_file.root.train_set.mor2f
train_mor3f = data_file.root.train_set.mor3f
train_atom_pairs = data_file.root.train_set.atom_pairs
train_topo_torsion = data_file.root.train_set.topo_torsion
train_topo = data_file.root.train_set.topo
train_maccs = data_file.root.train_set.maccs
train_structural = data_file.root.train_set.structural

test_phys_chem = data_file.root.test_set.phys_chem
test_mor1 = data_file.root.test_set.mor1
test_mor2 = data_file.root.test_set.mor2
test_mor3 = data_file.root.test_set.mor3
test_mor1f = data_file.root.test_set.mor1f
test_mor2f = data_file.root.test_set.mor2f
test_mor3f = data_file.root.test_set.mor3f
test_atom_pairs = data_file.root.test_set.atom_pairs
test_topo_torsion = data_file.root.test_set.topo_torsion
test_topo = data_file.root.test_set.topo
test_maccs = data_file.root.test_set.maccs
test_structural = data_file.root.test_set.structural

index_phys_chem = range(3)
index_var_mor1 = data_file.root.prepro.var_mor1[:]
index_var_mor2 = data_file.root.prepro.var_mor2[:]
index_var_mor3 = data_file.root.prepro.var_mor3[:]
index_var_mor1f = data_file.root.prepro.var_mor1f[:]
index_var_mor2f = data_file.root.prepro.var_mor2f[:]
index_var_mor3f = data_file.root.prepro.var_mor3f[:]
index_var_atom_pairs = data_file.root.prepro.var_atom_pairs[:]
index_var_topo_torsion = data_file.root.prepro.var_topo_torsion[:]
index_var_topo = data_file.root.prepro.var_topo[:]
index_var_maccs = data_file.root.prepro.var_maccs[:]
index_var_structural = data_file.root.prepro.var_structural[:]

train_data_info = data_file.root.train_set.data_info
train_classes = train_data_info.cols.mol_class[:]

test_data_info = data_file.root.test_set.data_info
test_classes = test_data_info.cols.mol_class[:]

train_data = [train_phys_chem, train_mor1, train_mor2, train_mor3, train_mor1f, train_mor2f, train_mor3f,
             train_atom_pairs, train_topo_torsion, train_topo, train_maccs, train_structural]

test_data = [test_phys_chem, test_mor1, test_mor2, test_mor3, test_mor1f, test_mor2f, test_mor3f, test_atom_pairs,
             test_topo_torsion, test_topo, test_maccs, test_structural]

names = ["physical-chemical descriptors",
         "ECFP2",
         "ECFP4",
         "ECFP6",
         "FCFP2",
         "FCFP4",
         "FCFP6",
         "atom pairs",
         "topological torsions",
         "RDKit",
         "MACCS",
         "MQN + number of chiral centers"]

indexes1 = [index_phys_chem, index_var_mor1, index_var_mor2, index_var_mor3, index_var_mor1f, index_var_mor2f,
            index_var_mor3f, index_var_atom_pairs, index_var_topo_torsion, index_var_topo, index_var_maccs,
            index_var_structural]

colors = ["#FFFF00", "blue", "red", "green", "magenta", "cyan", "black", "#66FF66", "#FF9933", "#CC33FF", "#800000",
          "#9900CC"]

bedroc_cols = ["bedroc100", "bedroc20", "bedroc10", "bedroc5", "bedroc3", "bedroc2"]
plt.figure()
perc = [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100]


ac = data(train_data[0], test_data[0], indexes1[0])
print ac
plt.plot(perc, ac, marker="+", color="%s" % colors[0], label='%s' % names[0])


ac = data(train_data[1], test_data[1], indexes1[1])
print ac
plt.plot(perc, ac, marker=".", color="%s" % colors[1], label='%s' % names[1])

ac = data(train_data[2], test_data[2], indexes1[2])
print ac
plt.plot(perc, ac, marker="o", color="%s" % colors[2], label='%s' % names[2])

ac = data(train_data[3], test_data[3], indexes1[3])
print ac
plt.plot(perc, ac, marker="*", color="%s" % colors[3], label='%s' % names[3])

ac = data(train_data[4], test_data[4], indexes1[4])
print ac
plt.plot(perc, ac, marker="p", color="%s" % colors[4], label='%s' % names[4])

ac = data(train_data[5], test_data[5], indexes1[5])
print ac
plt.plot(perc, ac, marker="s", color="%s" % colors[5], label='%s' % names[5])

ac = data(train_data[6], test_data[6], indexes1[6])
print ac
plt.plot(perc, ac, marker="x", color="%s" % colors[6], label='%s' % names[6])

ac = data(train_data[7], test_data[7], indexes1[7])
print ac
plt.plot(perc, ac, marker="D", color="%s" % colors[7], label='%s' % names[7])

ac = data(train_data[8], test_data[8], indexes1[8])
print ac
plt.plot(perc, ac, marker="h", color="%s" % colors[8], label='%s' % names[8])


ac = data(train_data[9], test_data[9], indexes1[9])
print ac
plt.plot(perc, ac, marker="^", color="%s" % colors[9], label='%s' % names[9])


ac = data(train_data[10], test_data[10], indexes1[10])
print ac
plt.plot(perc, ac, marker="o", color="%s" % colors[10], label='%s' % names[10])

ac = data(train_data[11], test_data[11], indexes1[11])
print ac
plt.plot(perc, ac, marker="o", color="%s" % colors[11], label='%s' % names[11])



plt.plot([0, 100], [0, 100], 'k--')
plt.xlim([0.0, 100.0])
plt.ylim([0.0, 100.0])
plt.xlabel('Percentage of test set screened',fontsize=18)
plt.ylabel('Percentage of natural products found', fontsize=18)
plt.title('Enrichment curves for each descriptor', fontsize=20)
plt.legend(loc="lower right", prop={'size': 20})
plt.show()

data_file.close()