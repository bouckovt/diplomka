import numpy as np
from tables import *
import tables
# preddefinovani velikosti pytables

# pocet train set 334365
# --pocet train NP 125942
# --pocet train nonNp 208423
# pocet test set 143298
# --pocet test NP 53974
# --pocet test nonNp 89324
# celkem 477663

number_of_rows = 477663
number_of_train_set = 334365
number_of_test_set = 143298

# popis dat pro pytable
class Labels(IsDescription):
    mol_class = tables.Int8Col() # 16-character String
    zinc_id = tables.StringCol(20)     # Signed 64-bit integer

fileh = open_file("data.h5", mode="w", title="Datasets for analysis")

group_train = fileh.create_group("/", 'train_set', 'Train set data')

group_test = fileh.create_group("/", 'test_set', 'Test set data')

# vztvoreni tabulky pro data info v obou grps
table = fileh.create_table(group_train, 'data_info', Labels, "Data description")
table2 = fileh.create_table(group_test, 'data_info', Labels, "Data description")

# vytvoreni poli pro kazdy fp/descriptor
# TRAIN SET
a = np.ones((number_of_train_set, 3), np.float32)
fileh.create_array(group_train, 'phys_chem', a, "Physical and chemical descriptors (mol_wt, mol_mr, logP)")

b = np.ones((number_of_train_set, 1024), np.int8)
fileh.create_array(group_train, 'mor1', b, "Morgan, 1024 bits, rad1, unfeatured")

c = np.ones((number_of_train_set, 1024), np.int8)
fileh.create_array(group_train, 'mor2', c, "Morgan, 1024 bits, rad2, unfeatured")

d = np.ones((number_of_train_set, 1024), np.int8)
fileh.create_array(group_train, 'mor3', d, "Morgan, 1024 bits, rad3, unfeatured")

e = np.ones((number_of_train_set, 1024), np.int8)
fileh.create_array(group_train, 'mor1f', e, "Morgan, 1024 bits, rad1, featured")

f = np.ones((number_of_train_set, 1024), np.int8)
fileh.create_array(group_train, 'mor2f', f, "Morgan, 1024 bits, rad2, featured")

g = np.ones((number_of_train_set, 1024), np.int8)
fileh.create_array(group_train, 'mor3f', g, "Morgan, 1024 bits, rad3, featured")

h = np.ones((number_of_train_set, 2048), np.int8)
fileh.create_array(group_train, 'atom_pairs', h, "Atom pairs")

i = np.ones((number_of_train_set, 2048), np.int8)
fileh.create_array(group_train, 'topo_torsion', i, "Topological torsion")

j = np.ones((number_of_train_set, 2048), np.int8)
fileh.create_array(group_train, 'topo', j, "Topological fp, Daylight ones")

k = np.ones((number_of_train_set, 167), np.int8)
fileh.create_array(group_train, 'maccs', k, "Maccs fp")

l = np.ones((number_of_train_set, 43), np.int16)
fileh.create_array(group_train, 'structural', l, "Structural desc, MQN + number of chiral centers")
#
#
# TEST SET
a2 = np.ones((number_of_test_set, 3), np.float32)
fileh.create_array(group_test, 'phys_chem', a2, "Physical and chemical descriptors (mol_wt, mol_mr, logP)")

b2 = np.ones((number_of_test_set, 1024), np.int8)
fileh.create_array(group_test, 'mor1', b2, "Morgan, 1024 bits, rad1, unfeatured")

c2 = np.ones((number_of_test_set, 1024), np.int8)
fileh.create_array(group_test, 'mor2', c2, "Morgan, 1024 bits, rad2, unfeatured")

d2 = np.ones((number_of_test_set, 1024), np.int8)
fileh.create_array(group_test, 'mor3', d2, "Morgan, 1024 bits, rad3, unfeatured")

e2 = np.ones((number_of_test_set, 1024), np.int8)
fileh.create_array(group_test, 'mor1f', e2, "Morgan, 1024 bits, rad1, featured")

f2 = np.ones((number_of_test_set, 1024), np.int8)
fileh.create_array(group_test, 'mor2f', f2, "Morgan, 1024 bits, rad2, featured")

g2 = np.ones((number_of_test_set, 1024), np.int8)
fileh.create_array(group_test, 'mor3f', g2, "Morgan, 1024 bits, rad3, featured")

h2 = np.ones((number_of_test_set, 2048), np.int8)
fileh.create_array(group_test, 'atom_pairs', h2, "Atom pairs")

i2 = np.ones((number_of_test_set, 2048), np.int8)
fileh.create_array(group_test, 'topo_torsion', i2, "Topological torsion")

j2 = np.ones((number_of_test_set, 2048), np.int8)
fileh.create_array(group_test, 'topo', j2, "Topological fp, Daylight ones")

k2 = np.ones((number_of_test_set, 167), np.int8)
fileh.create_array(group_test, 'maccs', k2, "Maccs fp")

l2 = np.ones((number_of_test_set, 43), np.int16)
fileh.create_array(group_test, 'structural', l2, "Structural desc, MQN + number of chiral centers")

table.flush()
table2.flush()
fileh.close()